basepairs = ['A','T','C','G']
def reverse_complement(s):
    complements = {'A':'T', 'T':'A', 'G':'C', 'C':'G'}
    return ''.join([complements[c] for c in reversed(s)])

def reverse_palindromes(s):
    results = []

    l = len(s)

    for i in range(l):
        for j in range(4, l):

            if i + j > l:
                continue

            s1 = s[i:i+j]
            s2 = reverse_complement(s1)

            if s1 == s2:
                results.append((i + 1, j))

    return results

seq = raw_input('What is your input DNA sequence? [A,T,C,G]')
seqL = len(seq)
for i in range (0, seqL):
	if seq[i] in basepairs:
		reverse_palindromes(seq)
	else:
		print "You've entered a non-DNA letter!"
		print "Run the script over and try again."
		print "Bye"
		exit()

#seq = "TCAATGCATGCGGGTCTATATGCAT" #check sequence
results = reverse_palindromes(seq)
print results
