#!/usr/bin/env python

"""
Prepare input file for 'edgedetect' v0.1

Author: Hyung Min Cho
        Icahn School of Medicine at Mount Sinai
         
input files:
        Aligned sequence file in SAM format

output:
        Profile of number of hits ('sum' data)

"""

#debug:
#import sys

#
# --- Process input parameters ---
#
import argparse
parser=argparse.ArgumentParser(description="Accumulate number of hits")
parser.add_argument("--maxlen","-m",type=int,default=100000000,
                    help="max. length of reference sequence (default=100000000)")
parser.add_argument("--chr","-c",type=str,default="chr1",
                    help="reference chromosome name (default=chr1)")
parser.add_argument("SAM_inp",type=str, 
                    help="(input) sequence file name in SAM format")
args=parser.parse_args()

print '# reference chromosome = ',args.chr
print '# input file name      = ',args.SAM_inp
print '#'
print '# --- \n'

#
# --- Process short reads and accumulate number of hits ---
#

#     Open input SAM file and initialize variables
file=open(args.SAM_inp,'r')
RNAseq=[0]*args.maxlen
ini=args.maxlen
fin=1

#     Read input file and accumulate number of hits
for lineIN in file:
    if lineIN[0] == '@':
        pass                                           # pass header section
    else:
        column=lineIN.split('\t')
        if column[2]==args.chr:                        # reference sequence name
            N=column[5]                                # CIGAR string
            if N[-1]=='M' and N[:-1].isdigit():        # read alignment matched sequences only
               SeqLen=int(N[:-1])                      # length of each read
               offset=int(column[3])                   # 1-based leftmost mapping position
               i=offset
               j=offset+SeqLen                         # 1-based rightmost mapping position + 1
               ini = i if i < ini else ini             # update the leftmost position
               fin = j if j > fin else fin             # update the rightmost position

               RNAseq[i:j]=[k+1 for k in RNAseq[i:j]]  # update number of hits

file.close()

#
# --- Print out 'sum' data ---  
#

for k in range(ini,fin):
    print '%s\t%s' % (k, str(RNAseq[k]))
