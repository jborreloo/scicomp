s='TCAATGCATGCGGGTCTATATGCAT'
r,l,o,f=range,len(s),[],[]
#this finds all the reverse palindromes
for a in[s[i:j+1]for i in r(l)for j in r(i,l)]:q=['TC GA'.index(c)-2for c in a];o+=[a if[-n for n in q][::-1]==q else'']
#this finds the positons of all the resver palindromes
for a in[s[i:j+1]for i in r(l)for j in r(i,l)]:q=['TC GA'.index(c)-2for c in a];
#print o #checking the master array
pal = [] #where all the palindromes from the master list (o) are going to go
#removing the empty spaces from the master list
for n in range(0,len(o)):
	if o[n] != '':
		pal.append(o[n])
#print pal #check to make sure we got rid of the spaces
#get rid of all the 2 bp palindromes (we want anything more than or equal to 4)
for m in range(0,len(pal)):
	if len(pal[m]) <= 3:
		pal[m] = ''
#print pal #check to make sure we got it right again
pal2 = [] #where all the 4+ bp palindromes are going
#getting rid of the spaces that were left behind removing the 2bp palindromes
for q in range(0,len(pal)):
	if pal[q] != '':
		pal2.append(pal[q])
#print pal2 #another check
pal_len = [] #where the palindrome lengths are going to go
for h in range(0, len(pal2)):
	pal_len.append(len(pal2[h]))
print pal_len #another check
