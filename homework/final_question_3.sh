#!/bin/bash
cd $HOME/scicomp/homework/bash_final/dir2
count=$(ls | wc -l)
#echo $count
for (( i=1; i<$count; i++ )); do
	echo $i
	if [ -e "file$i.out" ]; then
		:
	else
		bsub -q expressalloc -P acc_BSR1015F2015 -J "makeout.$i" -o "file$i.out" -n 1 -W 10 "cat file$i"
	fi
done
