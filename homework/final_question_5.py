basepairs = ['A','T','C','G']
inseq = raw_input('What is your input DNA sequence? [A,T,C,G]')
inseqL = len(inseq)
outseq = []
for i in range (0, inseqL):
	if inseq[i] in basepairs:
		if inseq[i] == 'A':
			outseq.append('T')
		elif inseq[i] == 'T':
			outseq.append('A')
		elif inseq[i] == 'C':
			outseq.append('G')
		else:
			outseq.append('C')
	else:
		print "You've entered a non-DNA letter!"
		print "Run the script over and try again."
		print "Bye"
		exit()

print ''.join(outseq)
