/*
   simple openmp parallel vector-vector addition.
   compile like: gcc -std=gnu99 -fopenmp program.c

   anthony b. costa
   bsr1015 class no. 3
   introduction to scientific computing
   september, 2015
*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>

#define safe_malloc(__ptr, __type, __size) { \
  __ptr = (__type *) malloc ((__size) * sizeof (__type)); \
  if (__ptr == NULL) { \
    fprintf (stdout, "memory allocation failed\n"); \
    exit (EXIT_FAILURE); \
  } \
}

/* multiple precision support. */
#if (defined DOUBLE_PRECISION)
  typedef double _myfp;
#else
  typedef float _myfp;
#endif /* DOUBLE_PRECISION */

int 
main (int argc, char **argv)
{
  /* check to make sure user supplied a vector length and a number
     of threads. */
  if (argc != 3)
  {
    fprintf (stdout, "usage:\n");
    fprintf (stdout, "./a.out <vector_length> <n_threads>\n");
    exit (EXIT_FAILURE);
  }

  /* define the size of our vector and the number of threads. */
  const unsigned int length = atoi (argv[1]);
  const unsigned int n_threads = atoi (argv[2]);
  omp_set_num_threads (n_threads);

  /* check to make sure this much memory is really ok.
     artificial limit is set to 8 GB. */
  double footprint = ((length * sizeof (_myfp)) / 1024 / 1024) * 3;

  #if (defined DOUBLE_PRECISION)
    fprintf (stdout, "will work in double precision...\n");
  #else
    fprintf (stdout, "will work in single precision...\n");
  #endif /* DOUBLE_PRECISION */

  fprintf (stdout, "want to add %d elements...\n", length);
  fprintf (stdout, "will run with %d threads...\n", n_threads);
  fprintf (stdout, "will allocate %.2f MB of memory...\n", footprint);
  if (footprint > 8192)
  {
    fprintf (stdout, "program is limited to 8 GB of memory, sorry...\n");
    exit (EXIT_FAILURE);
  }

  /* allocate memory dynamically and check to make sure it worked. */
  _myfp *vector_a, *vector_b, *vector_c;
  safe_malloc (vector_a, _myfp, length);
  safe_malloc (vector_b, _myfp, length);
  safe_malloc (vector_c, _myfp, length);

  /* we're going to measure some timing information. */
  struct timeval tv1, tv2;

  /* loop over all length of the vector in parallel and assign
     a random value in the range [0, 1). */
  fprintf (stdout, "assigning random values to vector elements...\n");

  gettimeofday (&tv1, NULL);
  #pragma omp parallel
  {
    unsigned int seed = omp_get_thread_num ();
    
    #pragma omp for
    for (unsigned int i = 0; i < length; i++) {
      vector_a [i] = rand_r (&seed) / (_myfp) RAND_MAX;
      vector_b [i] = rand_r (&seed) / (_myfp) RAND_MAX;
    }
  }

  gettimeofday (&tv2, NULL);
  fprintf (stdout, "random number generation took %f milliseconds...\n", \
    (double) ((tv2.tv_usec - tv1.tv_usec) / 1e3) + \
    (double) ((tv2.tv_sec - tv1.tv_sec) * 1e3));

  fprintf (stdout, "adding vectors...\n");

  gettimeofday (&tv1, NULL);
  #pragma omp parallel for
  for (unsigned int i = 0; i < length; i++)
    vector_c [i] = vector_a [i] + vector_b [i];

  gettimeofday (&tv2, NULL);
  fprintf (stdout, "vector-vector addition took %f milliseconds...\n", \
    (double) ((tv2.tv_usec - tv1.tv_usec) / 1e3) + \
    (double) ((tv2.tv_sec - tv1.tv_sec) * 1e3));

  /* we're done! free the memory we used. */
  free (vector_a);
  free (vector_b);
  free (vector_c);

  /* exit the function with information about how it went
     (in this case, successfully). */
  fprintf (stdout, "done! will now exit...\n");
  return (EXIT_SUCCESS);
}
