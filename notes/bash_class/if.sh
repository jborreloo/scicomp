#!/bin/bash

# Example of the use of the if construct
ascii_file=0
executable=0
other=0

for f in `ls ~`
do
	if  file ~/$f | grep -q ASCII ; then

		(( ascii_file++ ))

	elif [ -x ~/$f ]; then
		
		(( executable+=1 ))
	else
		other=$[$other+1]
	fi
done

echo -e "As of \c";date
echo "$ascii_file ASCII files"
echo "$executable executable files"
echo "$other neither type"
		
