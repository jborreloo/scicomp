#!/bin/bash

# Demo program to count up to 1000 with pauses in between

[ "$1" == "" ]  && st=10 || st=$1 

i=0				# Initialize counter

while [ $i -lt 1000 ]		# Loop until 10000
do
	echo "PID $$ count $i"
	i=$[$i+1]		# Note how arithmetic is done
	sleep $st			# Pause
done				# End of loop
