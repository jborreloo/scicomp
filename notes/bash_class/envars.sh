#!/bin/bash

# Define environment variables

# First echo the parent shell variable

echo "ParentVar in script=$ParentVar"

export ChildVar="Only if you believe it is."

echo "ChildVar in script=$ChildVar"
