#!/bin/bash
# csplit.sh = with comments

# Split the fasta file and remember how many files were created

n_files=`csplit -z fasta.txt /\>/ {*} | wc -l`
echo "$n_files files created by csplit"

# Files numbered starting at 0 so last sequence number is one less
#	than number of files.
last_seq=$(($n_files-1))

# Loop over sequence numbers remembering to pad 0-9 with a 0
# Run command in bacground

for i in `seq 0 $last_seq`
do
	if [ $i -lt 10 ]; then
		fname="xx0$i"
	else
		fname="xx$i"
	fi
	echo "cat $fname > /dev/null &" | tee >(cat >> csplit.log) | sh 
done

# And wait
wait
echo "Done."


