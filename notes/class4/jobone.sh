#!/bin/bash

#BSUB -P acc_BSR1015
#BSUB -q expressalloc
#BSUB -n 2
#BSUB -R "span[hosts=1]"
#BSUB -W 2
#BSUB -o stdout.%J.%I
#BSUB -e stderr.%J.%I

cd ~/scicomp/notes/openmp/
./a.out 100000000 2
