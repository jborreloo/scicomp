#!/bin/bash

grep "vector-vector addition" stdout* | cut -d " " -f 4 | cut -d "." -f 1 | sort -g | uniq -c | sort -g -k 2
echo average:
grep "vector-vector addition" stdout* | cut -d " " -f 3 | cut -d "." -f 1 | sort -g | awk '{sum+=1; total+=1} END {print sum/total}'
