/*
   bare-bones vector-vector addition.
   compile like: gcc -std=gnu99 program.c

   anthony b. costa
   bsr1015 class no. 3
   introduction to scientific computing
   september, 2015
*/

#include <stdlib.h>

int 
main (int argc, char **argv)
{
  //set up three empty vectors of length 1000	
  int length = 1000;
  float vector_a [length];
  float vector_b [length];
  float vector_c [length];

  //put random numbers into vectors a & b (the ones we're adding
  for (int i = 0; i < length; i++) {
    vector_a[i] = rand () / (float) RAND_MAX;
    vector_b[i] = rand () / (float) RAND_MAX;
  }

  //add element n from vector a to element n from vector b and get an answer to put into element n of vector c
  for (int i = 0; i < length; i++)
    vector_c[i] = vector_a[i] + vector_b[i];

  return (EXIT_SUCCESS);
}
